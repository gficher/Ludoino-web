<?php
header("Content-Type: application/json");

$mysqli = new mysqli("143.106.241.1", "3infd12", "3infd12", "3infd12");

if (isset($_GET['action'])) {
	$action = $_GET['action'];
} else {
	$action = "none";
}

$result = Array();

if ($action == "test") {
	$result['status'] = "ok";
	$result['message'] = "Test successful made!";
} else if ($action = "createGame") {
    $qry = $mysqli->prepare("SELECT id FROM ino_games WHERE conn_code = ? AND status <> 0");
    $qry->bind_param('s', $qry)
} else if ($action == "getGame") {
	if (isset($_GET['id'])) {
		$id = $_GET['id'];

		$qry = $mysqli->prepare("SELECT id, conn_code, status, creation_date, question FROM ino_games WHERE conn_code = ? and status != 0");
		$qry->bind_param('s', $id);
		$qry->execute();

		$r = Array();
		$qry->bind_result($r['id'], $r['conn_code'], $r['status'], $r['creation_date'], $r['question']);

		$qry->fetch();
		$qry->close();

		if (!empty($r['id'])) {

			$qry = $mysqli->prepare("SELECT game, player, position, last_changed FROM ino_players WHERE game = ?");
			$qry->bind_param('i', $r['id']);
			$qry->execute();
			$qry->bind_result($p['game'], $p['player'], $p['position'], $p['last_changed']);

			while($qry->fetch()) {
				$r['players'][] = Array(
					'player' => $p['player'],
					'position' => $p['position'],
					'last_changed' => $p['last_changed']
				);
			}

			$qry->close();

			$result['status'] = "ok";
			$result['info'] = $r; 
		} else {
			$result['status'] = "error";
			$result['message'] = "Game not found";
		}
	} else {
            $result['status'] = "error";
            $result['message'] = "Please provide a game ID";
	}
} else if ($action == "setQuestion") {
	$result['status'] = "ok";
        if ((isset($_GET['id'])) and ($_GET['question'])) {
            $id = $_GET['id'];
            $question = $_GET['question'];
            
            $qry = $mysqli->prepare("UPDATE ino_games SET question = ? WHERE id = ?");
        } else {
            $result['status'] = "error";
            $result['message'] = "Please provide a game ID";
        }
} else if ($action == "") {
	$result['status'] = "ok";
} else if ($action == "") {
	$result['status'] = "ok";
} else if ($action == "") {
	$result['status'] = "ok";
} else if ($action == "") {
	$result['status'] = "ok";
} else {
	$result['status'] = "error";
	$result['message'] = "Please enter an action";
}

echo json_encode($result, JSON_PRETTY_PRINT);